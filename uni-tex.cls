% ------
% This is a handout document class. It is based on the article class. It is design for creating handouts for a college class
% Created By: Roger Cortesi, 27 JUN 2007
% ----
% ---- Identification -----

%\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{handout}

% ---- Initial Code ----

\RequirePackage{ifthen}


% Schrift & Sprache
\RequirePackage[utf8]{luainputenc}
\RequirePackage[T1]{fontenc} % T1-Fonts
\RequirePackage[ngerman]{babel} % Deutsche Sonderzeichen und Silbentrennung (neue Rechtschreibung)
\usepackage{mathpazo}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage{color}
\RequirePackage{pifont} % einige Befehle brauchen pifont (Symbole)
\RequirePackage{setspace}
\RequirePackage[style=authortitle-dw, backend=bibtex]{biblatex}
\usepackage[a4paper,bottom=15mm]{geometry} % für Seitenränder,
\usepackage{graphicx}

%from uni-tex
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{tikz}
%\usepackage{float}
\usepackage{ifthen} 
%\usepackage[shortlabels]{enumitem}
\usepackage{float}

%stuff
\usepackage[a4paper]{geometry}
%\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}

%drawing automata
\usepackage {tikz}
\usetikzlibrary {arrows , automata , positioning}

%drawing charts
\usepackage{pgfplots}

%fancy algorithms
\usepackage[linesnumbered,ruled,noend]{algorithm2e}

%structure look at the end of the document




\pagenumbering{arabic}

% ---- Execution of Options ----

\ProcessOptions \relax

% --- Package Loading ----

%\LoadClass[10pt]{scrartcl}
\LoadClass{article}

% ---- Main Code ----

% --- Setup the Page Layout ---
	\setlength\textwidth{170mm}
	\setlength\textheight{220mm}
	\setlength\oddsidemargin{-5mm}
	\setlength\evensidemargin{0in}
	\setlength\topmargin{-0.25in}
	\setlength\parindent{0in}
	\setlength\parskip{0.5em}

% --- Define Some Useful Commands ---
	% Define the default values for these commands
	\newcommand{\University}{}
	\newcommand{\Title}{}
	\newcommand{\Datum}{}
	\newcommand{\Fakultaet}{}
	\newcommand{\Semester}{}
	\newcommand{\Dozent}{}
	\newcommand{\People}{}
	\newcommand{\Matrikelnr}{}
	\newcommand{\Group}{}
	\newcommand{\Blank}{}

	% change the values if the user sets these commands
	\newcommand{\SetTitle}[1]{\renewcommand{\Title}{#1}}
	\newcommand{\SetUniversity}[1]{\renewcommand{\University}{#1}}
	\newcommand{\SetFakultaet}[1]{\renewcommand{\Fakultaet}{#1}}
	\newcommand{\SetSemester}[1]{\renewcommand{\Semester}{#1}}
	\newcommand{\SetDozent}[1]{\renewcommand{\Dozent}{#1}}
	\newcommand{\SetPeople}[1]{\renewcommand{\People}{#1}}
	\newcommand{\SetMatrikelnr}[1]{\renewcommand{\Matrikelnr}{#1}}
	\newcommand{\SetDatum}[1]{\renewcommand{\Datum}{#1}}
	\newcommand{\SetGroup}[1]{\renewcommand{\Group}{#1}}
	\newcommand{\SetLiteratur}[1]{\bibliography{#1}}
	
	\newcommand{\makeliteratur}{
	
		\singlespacing
		\nocite{*}
		\printbibliography
	}
	
	% redefine the make title command
	\renewcommand\maketitle{
		\thispagestyle{title}
		\vspace*{-5mm} %space between header and title
		\begin{center}
		\LARGE{\Title} \\
		\end{center}
		\onehalfspacing 
	}

	% Redefine the sectioning commands \section \subsection and \subsubsection
	%\renewcommand\section{\@startsection
	%	{section}{2}{0mm}{0.1\baselineskip}{0.1\baselineskip}{\normalfont\large\textbf}
	%}
	%\renewcommand\subsection{\@startsection
	%	{subsection}{2}{0mm}{0.1\baselineskip}{0.1\baselineskip}{\normalfont\normalsize\textbf}
	%}
	%\renewcommand\subsubsection{\@startsection
	%	{subsubsection}{2}{0mm}{0.1\baselineskip}{0.1\baselineskip}{\normalfont\normalsize\textbf}
	%}
	%\newenvironment{noindlist}
	% {\begin{list}{\labelitemi}{\leftmargin=1em\itemindent=0em\topsep=-5em\itemsep=-4pt}}
	% {\end{list}}
	 

% --- Define the Header and Footer Styles ---

	% The style for the page with \maketitle called on it.
	\fancypagestyle{title} {
		\renewcommand\headrulewidth{0.5pt}
		\renewcommand\footrulewidth{0.5pt}
		\chead{}
		\cfoot{\thepage\ of \pageref{LastPage}}
		\lhead{ \small
				\textbf{\University}\\
				\textit{\Fakultaet}\\
				\Group\\
				\Dozent
                                \People
			  }	
		\lfoot{}
		\rhead{%\Blank 
				\small
                                \includegraphics[width=1.5cm]{/usr/share/uni-tex/uni-logo}\\
				%Matrikelnummer: \Matrikelnr\\
				\Datum
				\Semester
			  }
		\addtolength{\headheight}{5mm}
	}

	% The style for pages without \maketitle called on it.
	\fancypagestyle{template} {
		\renewcommand\headrulewidth{0.5pt}
		\renewcommand\footrulewidth{0.5pt}
		\chead{}
		\cfoot{\thepage\ of \pageref{LastPage}}
		\rhead{ \small \Datum \\ \People}
		\lhead{ \small \Group \\ \Title}
		\lfoot{}
	}
	
% Set the default page style to handout.
	\pagestyle{template}

% Load at last
\RequirePackage[babel, german=quotes]{csquotes}
\RequirePackage{longtable}
\RequirePackage{multirow}
\usepackage[
   % Farben fuer die Links
   colorlinks=true,         % Links erhalten Farben statt Kaeten
   urlcolor=black,    % \href{...}{...} external (URL)
   filecolor=black,  % \href{...} local file
   linkcolor=black,  % \ref{...} and \pageref{...}
   citecolor=black,
   menucolor=black,
   % Links
   raiselinks=true,			 % calculate real height of the link
   breaklinks,              % Links berstehen Zeilenumbruch
   verbose,
   hyperfootnotes=false,    % Keine Links auf Fussnoten
   % Anchors
   plainpages=false,        % Anchors even on plain pages ?
   pageanchor=true,         % Pages are linkable
   pdfstartview=FitH,       % Dokument wird Fit Width geoeffnet
]{hyperref}

%OLD STUFF

\def\sheetlanguageGerman{german}
\def\sheetlanguageEnglish{english}

\def\@sheetlanguage{\sheetlanguageGerman}
\DeclareOption{en}{\def\@sheetlanguage{\sheetlanguageEnglish}}
\DeclareOption{de}{\def\@sheetlanguage{\sheetlanguageGerman}}
\ProcessOptions\relax

\newcommand{\lang}[2]{\ifthenelse{\equal{\@sheetlanguage}{\sheetlanguageGerman}}{#1}{#2}}

\newcommand{\lecture}[1]{\def\@lecture{#1}}
\def\@lecture{}

\newcommand{\group}[1]{\def\@group{#1}}
\def\@group{}

\newcommand{\sheet}[1]{\def\@sheet{#1}}
\def\@sheet{}

\newcommand{\prof}[1]{\def\@prof{#1}}
\def\@prof{}

\newcommand{\sheetname}[1]{\def\@sheetname{#1}}
\def\@sheetname{\lang{Hausaufgabe}{Exercise sheet}}

\newcommand{\annotate}[1]{&& \left|\  #1 \right.}
\newcommand{\annotatet}[1]{\annotate{\text{#1}}}

%\chead{\Large \textbf{\@sheetname \, \@sheet}}
%\lhead{\@author \\ \lang{Universität des Saarlandes}{Saarland University}}
%\rhead{
%  \@lecture \\
%  \@prof \\
%  \ifx\@group\empty\else \lang{Tutorium}{Tutorial} \@group\fi
%}
%\cfoot{\thepage/\pageref{LastPage}}
%\pagestyle{fancy}

% Text über dem Equiv:
% \newcommand\equivt[1]{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily #1}}}{\equiv}}}
\newcommand\equalt[1]{\stackrel{\mathclap{\normalfont\mbox{#1}}}{\hspace{1.3cm}=\hspace{1.3cm}}}
\newcommand\equivt[1]{\stackrel{\mathclap{\normalfont\mbox{#1}}}{\hspace{1.3cm}\equiv\hspace{1.3cm}}}

\newcommand\TODO{
	\begin{LARGE}
		\colorbox{red}{TODO}
	\end{LARGE}
}

\date{}
%\title{\lectureandgroup\ - \@sheetname\ \@sheet}


%\newcommand{\task}[1]{\section*{#1}}
%\newcommand{\subtask}[1]{\subsection*{#1}}
%\newcommand{\subsubtask}[1]{\subsection*{#1}}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}

%structure this NEEDS to be HERE dont move this if you're not a genius
\usepackage{enumitem}
