#!/bin/bash
if ! [ $(id -u) = 0 ]; then
   echo "Please run as root."
   exit 1
fi

mkdir /usr/share/uni-tex
cp -r * /usr/share/uni-tex
cp template/example.tex ~/Templates/uni-tex.tex
echo "Successfully installed."
